//
//  CapturePreviewView.swift
//  CoreMLVideo
//
//  Created by iulian david on 05/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit
import AVFoundation

class CapturePreviewView: UIView {

    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
}
