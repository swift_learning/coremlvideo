//
//  ViewController.swift
//  CoreMLVideo
//
//  Created by iulian david on 05/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit
import AVKit

enum SetupError: Error {
    case noVideoDevice, videoInputFailed, videoOutputFailed
}
class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    let session = AVCaptureSession()
    let videoOutput = AVCaptureVideoDataOutput()
    let capturePreview = CapturePreviewView()
    
    var assetWriter: AVAssetWriter!
    var writerInput: AVAssetWriterInput!
    
    let model = SqueezeNet()
    let context = CIContext()
    
    var recordingActive = false
    var readyToAnalyze = true
    var startTime: CMTime!
    var movieURL: URL!
    
    var predictions = [(time: CMTime, prediction: String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create Auto Layout constraints pinning it to all four edeges of out view
        capturePreview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(capturePreview)
        
        capturePreview.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        capturePreview.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        capturePreview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        capturePreview.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
        // tell it to attach to our existing session
        (capturePreview.layer as! AVCaptureVideoPreviewLayer).session = session
        
        do {
            // attempt to configure the session
            try configureSession()
            
            //if it worked, add a "Record" button
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Record", style: .plain, target: self, action: #selector(startRecording))
        } catch {
            print("Session configuration failed")
        }
    }
    
    func configureSession() throws {
        session.beginConfiguration()
        try configureVideoDeviceInput()
        try configureVideoDeviceOutput()
        try configureMovieWriting()
        session.commitConfiguration()
    }

    
    
    func configureVideoDeviceInput() throws {
        // find the default video capture or throw an error
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            throw SetupError.noVideoDevice
        }
        
        //create a device input from it (may also throw an error)
        let videoDeviceInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        
        //add it to our recording session or throw an error
        if session.canAddInput(videoDeviceInput) {
            session.addInput(videoDeviceInput)
        } else {
            throw SetupError.videoInputFailed
        }
    }
    
   
    func configureVideoDeviceOutput() throws {
        if session.canAddOutput(videoOutput) {
            
            //configure this view controller to receive data packets
            videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue.main)
            session.addOutput(videoOutput)
            
            // force portrait recording
            for connection in videoOutput.connections {
                for port in connection.inputPorts {
                    if port.mediaType == .video {
                        connection.videoOrientation = .portrait
                    }
                }
            }
        } else {
            throw SetupError.videoOutputFailed
        }
    }
    
    func configureMovieWriting() throws {
        movieURL = getDocumentDirectory().appendingPathComponent("movie.mov")
        let fm = FileManager.default
        
        if fm.fileExists(atPath: movieURL.path) {
            try fm.removeItem(at: movieURL)
        }
        
        // tell our asset writer where to save
        assetWriter = try AVAssetWriter(url: movieURL, fileType: .mp4)
        
        //figure out the best settings for writing MP4 files
        let settings = videoOutput.recommendedVideoSettingsForAssetWriter(writingTo: .mp4)
        
        //create a writer using those settings, and configure it for real-time video
        writerInput = AVAssetWriterInput(mediaType: .video, outputSettings: settings)
        writerInput.expectsMediaDataInRealTime = true
        
        //add the video recorder to main recorder, so we're good to go
        if assetWriter.canAdd(writerInput) {
            assetWriter.add(writerInput)
        }
    }
    
    @objc func startRecording() {
           recordingActive = true
           session.startRunning()
           
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Stop", style: .plain, target: self, action: #selector(stopRecording))
       }
    
    private func getDocumentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard recordingActive else { return }
        guard CMSampleBufferDataIsReady(sampleBuffer) == true else { return }
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        
        let currentTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        
        if assetWriter.status == .unknown {
            
            //store this away so we can calculate offsets later
            startTime = currentTime
            
            
            //start writing data to disk
            assetWriter.startWriting()
            assetWriter.startSession(atSourceTime: currentTime)
            
            //we're done for now now, so exit
            return
        }
        
        if assetWriter.status == .failed {
            // ykes
            return
        }
        if assetWriter.status == .writing  {
            if writerInput.isReadyForMoreMediaData {
                writerInput.append(sampleBuffer)
            }
        }
        
        guard readyToAnalyze else {
            return
        }
        
        readyToAnalyze = false
        
        DispatchQueue.global().async {
            
            //set our taget scale size
            let inputSize = CGSize(width: 227.0, height: 227.0)
            let image = CIImage(cvImageBuffer: pixelBuffer)

            //create a CVPixelBuffer at the smaller size
            guard let resizedPixelBuffer = image.pixelBuffer(at: inputSize, context: self.context) else { return }
            
            //pass it to CoreML to identify an object
            let prediction = try? self.model.prediction(image: resizedPixelBuffer)
            let predictionName = prediction?.classLabel ?? "Unknown"

            print("\(self.predictions.count): \(predictionName)")
            
            // figure out how much time has passed in the video
            let timeDiff = currentTime - self.startTime
            self.predictions.append((timeDiff, predictionName))
            self.readyToAnalyze = true
        }
        
    }
    
    @objc func stopRecording() {
        recordingActive = false
        
        writerInput.markAsFinished()
        
        assetWriter.finishWriting {
            if self.assetWriter.status == .failed {
                print("Creating movie file failed")
            } else {
                print("Creating movie file was a success")
                DispatchQueue.main.async {
                    let results = ResultsViewController(style: .plain)
                    results.movieURL = self.movieURL
                    results.predictions = self.predictions
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Record", style: .plain, target: self, action: #selector(self.startRecording))
                    self.navigationController?.pushViewController(results, animated: true)
                }
            }
        }
    }
}


extension CIImage {
    func pixelBuffer(at size: CGSize, context: CIContext) -> CVPixelBuffer? {
        let attributes = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary

        var pixelBuffer: CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(size.width), Int(size.height), kCVPixelFormatType_32ARGB, attributes, &pixelBuffer)
        guard status == kCVReturnSuccess else { return nil }

        let scale = size.width / self.extent.size.width
        let resizedImage = self.transformed(by: CGAffineTransform(scaleX: scale, y: scale))

        let width = resizedImage.extent.width
        let height = resizedImage.extent.height
        let yOffset = (CGFloat(height) - size.height) / 2.0
        let rect = CGRect(x: (CGFloat(width) - size.width) / 2.0, y: yOffset, width: size.width, height: size.height)
        let croppedImage = resizedImage.cropped(to: rect)
        let translatedImage = croppedImage.transformed(by: CGAffineTransform(translationX: 0, y: -yOffset))

        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        context.render(translatedImage, to: pixelBuffer!)
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))

        return pixelBuffer
    }
}
